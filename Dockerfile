FROM alpine:3.15

ENV USER app
ENV UID 1000
ENV GID 1000
ENV PORT 8080
ENV PAYLOAD_DIR /app/payload
ENV WORKDIR /app
WORKDIR ${WORKDIR}}
EXPOSE ${PORT}


RUN set -ex; \
  echo Update alpine linux packages; \
  apk update && \
  apk upgrade --no-cache; \
  echo Install packages that will stay with the image; \
  apk add --no-cache \
  curl \
  python3 \
  tzdata; \
  echo Create a service account; \
  adduser -D  -h ${WORKDIR}} -u ${UID} ${USER} ${USER};

ADD  --chown=/app:app server.py ${WORKDIR}}
CMD ["sh", "-c", "python3 server.py -p ${PORT} -d ${PAYLOAD_DIR}"]