import http.server
from http.server import BaseHTTPRequestHandler, HTTPServer
import os
import sys
import socketserver
import json
import argparse
from datetime import datetime

parser = argparse.ArgumentParser(description="Run a simple HTTP server")
parser.add_argument('-p','--port', metavar='8080', type=int, default=8080, help='Port to listen on')
parser.add_argument('-d','--payload-dir',  metavar='payload', default="payload", help='Directory to store payload files')
args = parser.parse_args()

OUT_HTML = """
<html>
<head>
  <title>Python 3 http.server</title>
</head>
<body>
<h1>Python 3 http.server</h1>
<h3>This is the index</h3>
</body>
"""
OUT_JSON = """
{
 "title": "python3 http.server",
 "test": true
}
"""

class S(BaseHTTPRequestHandler):
    def _find_in_headers(self, find_header, return_hvalue=False):
        hvalue_return = None
        bvaalue_return = False
        for hname, hvalue in self.headers.items():
            if hname.lower() == find_header.lower():
                hvalue_return = hvalue
                bvaalue_return = True
                break
        if return_hvalue:
            return hvalue_return
        else:
            return bvaalue_return

    def _set_headers(self):
        self.send_response(200)
        self.content_type = 'text/html'
        header_value = self._find_in_headers('accept', True)
        if header_value and header_value in ["application/json", "text/html"]:
            self.content_type = header_value

        self.send_header('content-type', self.content_type)
        self.end_headers()

        print("########## [Request Headers] ##########")
        for header, value in self.headers.items():
            print(f"- {header}: {value}")
        print("#######################################")
    
    def _send_output(self):
        content_length = self._find_in_headers('content-length', True)
        if content_length:
            self._get_payload(content_length)
        else:
            self._set_headers()
            if self.content_type == 'application/json':
                # f = open("index.json", "rb")
                outdata = OUT_JSON
            else:
                # f = open("index.html", "rb")
                outdata = OUT_HTML
            # self.wfile.write(f.read())
            # f.close()
            self.wfile.write(outdata.encode('utf-8'))

    def _write_payload(self, payload):
        payload_dir = args.payload_dir
        if not os.path.exists(payload_dir):
            os.makedirs(payload_dir)
        dt = datetime.now().strftime("%Y%m%d_%H%M%S")
        filename = f"{payload_dir}/{self.command.lower()}-payload_{dt}.txt"
        with open(filename, "w") as outfile:
            outfile.write(f"COMMAND: {self.command}\n")
            outfile.write(f"PATH: {self.path}\n")
            outfile.write(f"CLIENT: {self.client_address}\n")
            outfile.write(f"USER-AGENT: {self.headers['user-agent']}\n")
            outfile.write("\n[REQUEST HEADERS]\n")
            for header, value in self.headers.items():
                outfile.write(f"{header}: {value}\n")
            outfile.write("\n[PAYLOAD]\n")
            outfile.write(payload.decode('utf-8'))
        print(f"Payload written to {filename}")


    def _get_payload(self, content_length=None):
        self._set_headers()
        payload = self.rfile.read(int(content_length))
        self._write_payload(payload)
        self.wfile.write(payload)

    def do_GET(self):
        self._send_output()
    
    def do_POST(self):
        self._send_output()
    
    def do_PUT(self):
        self._send_output()

    def do_OPTIONS(self):
        self._set_headers()
        self.wfile.write("--OPTIONS--".encode('utf-8'))

    def do_HEAD(self):
        self._set_headers()
        self.wfile.write("--HEAD--".encode('utf-8'))


http = socketserver.TCPServer(("", args.port), S)


if __name__ == "__main__":
    print("serving at port", args.port)
    try:
        http.serve_forever()
    except KeyboardInterrupt:
        print("Shutting down the http server")
        pass
    http.server_close()

