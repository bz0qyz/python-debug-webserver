# Debug Web Server
A lightweight python script that will serve static content write post payload to a file for testing.

## Usage
```bash
python server.py -p 8080 -d /path/to/payload/files
```